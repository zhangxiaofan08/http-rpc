package com.haha.http.rpc.common.exception;

/**
 * @AUTHOR zhangxiaofan07
 * @CREATE 2022-02-09 19:45
 */
public class RpcCommonException extends RuntimeException {
    private static final long serialVersionUID = -2366100511645697743L;
    private final String code;
    private final String message;

    public RpcCommonException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
