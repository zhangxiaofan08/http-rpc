package com.haha.http.rpc.common.trace;

import com.haha.http.rpc.common.util.TraceUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @AUTHOR zhangxiaofan07
 * @CREATE 2022-01-28 11:32
 */
public class TraceContextHolder {
    private static final ThreadLocal<String> TRACE_THREAD_LOCAL = new ThreadLocal<>();

    public static String getTraceId() {
        String traceId = TRACE_THREAD_LOCAL.get();
        if (StringUtils.isBlank(traceId)) {
            TraceUtils.generateTraceId();
        }
        return TRACE_THREAD_LOCAL.get();
    }

    public static void setTraceId(String traceId) {
        TRACE_THREAD_LOCAL.set(traceId);
    }

    public static void clear() {
        TRACE_THREAD_LOCAL.remove();
    }
}
