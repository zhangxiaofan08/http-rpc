package com.haha.http.rpc.common.util;

import org.slf4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @AUTHOR zhangxiaofan07
 * @CREATE 2022-01-27 20:01
 */
public class LogUtils {
    public static void error(Logger logger, String msg, Throwable e) {
        logger.error("{}, e = {}", msg, getStackTrace(e));
    }

    public static void warn(Logger logger, String msg, Throwable e) {
        logger.warn("{}, e = {}", msg, getStackTrace(e));
    }

    private static String getStackTrace(Throwable throwable) {
        PrintWriter printWriter = null;
        try {
            StringWriter stringWriter = new StringWriter();
            printWriter = new PrintWriter(stringWriter, true);
            throwable.printStackTrace(printWriter);

            return stringWriter.getBuffer().toString();
        } catch (Exception e) {
            return null;
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }
}
