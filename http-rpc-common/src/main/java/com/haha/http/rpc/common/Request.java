package com.haha.http.rpc.common;

import java.io.Serializable;
import java.util.Map;

/**
 * @AUTHOR zhangxiaofan07
 * @CREATE 2022-01-27 20:26
 */
public class Request implements Serializable {

    private static final long serialVersionUID = 5448139471407573586L;

    private String traceId;
    private Class<?> objType;
    private String methodName;

    private Map<Class<?>, Object> paramMap;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Map<Class<?>, Object> getParamMap() {
        return paramMap;
    }

    public void setParamMap(Map<Class<?>, Object> paramMap) {
        this.paramMap = paramMap;
    }

    public Class<?> getObjType() {
        return objType;
    }

    public void setObjType(Class<?> objType) {
        this.objType = objType;
    }

    @Override
    public String toString() {
        return "Request{" +
                "traceId='" + traceId + '\'' +
                ", objType=" + objType +
                ", methodName='" + methodName + '\'' +
                ", paramMap=" + paramMap +
                '}';
    }
}
